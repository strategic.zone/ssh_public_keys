#!/usr/bin/env bash
#title		    : install_pubkey_iuri.sh
#description	: Installation de la cle public de Iuri
#author		    : Valeriu Stinca
#email		    : ts@strategic.zone
#date		    : 13092020
#version	    : 0.3
#notes	    	:
#===================

key_url="https://gitlab.com/strategic.zone/ssh_public_keys/raw/master/id_ecdsa_iuri-laptop.pub"

mkdir -p ~/.ssh
grep -q "$(curl --silent "${key_url}")" ~/.ssh/authorized_keys || curl --silent "${key_url}" | tee -a ~/.ssh/authorized_keys
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys