#!/usr/bin/env bash
#title		    : install_pubkey_valerius.sh
#description	: Installation de la cle public de Strategic Zone
#author		    : Valeriu Stinca
#email		    : ts@strategic.zone
#date		    : 28-11-2019
#version	    : 0.3
#notes	    	:
#===================

key_url="https://gitlab.com/strategic.zone/ssh_public_keys/raw/master/id_ecdsa_valerius_ed25519.pub"

mkdir -p ~/.ssh
grep -q "$(curl --silent "${key_url}")" ~/.ssh/authorized_keys || curl --silent "${key_url}" | tee -a ~/.ssh/authorized_keys
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys
