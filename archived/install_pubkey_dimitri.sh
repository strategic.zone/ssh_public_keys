#!/usr/bin/env bash
#title		    : install_pubkey_dimitri.sh
#description	: Installation de la cle public du laptop de dimitri
#author		    : Valeriu Stinca
#email		    : ts@strategic.zone
#date		    : 10-05-2019
#version	    : 0.2
#notes	    	:
#===================

mkdir -p ~/.ssh
curl --silent https://gitlab.com/strategic.zone/ssh_public_keys/raw/master/id_ecdsa_dimitri_laptop.pub | tee -a ~/.ssh/authorized_keys
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys
