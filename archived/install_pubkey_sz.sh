#!/usr/bin/env bash
#title		    : install_pubkey_sz.sh
#description	: Installation de la cle public de Strategic Zone
#author		    : Valeriu Stinca
#email		    : ts@strategic.zone
#date		    : 20181210
#version	    : 0.2
#notes	    	:
#===================

mkdir -p ~/.ssh
curl --silent https://gitlab.com/strategic.zone/ssh_public_keys/raw/master/id_rsa_strategiczone.pub | tee -a ~/.ssh/authorized_keys
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys

