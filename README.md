# Ajout de la clé ssh

```sh
mkdir -p ~/.ssh
curl --silent https://gitlab.com/strategic.zone/ssh_public_keys/raw/master/id_rsa_strategiczone.pub | tee -a ~/.ssh/authorized_keys
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys
```